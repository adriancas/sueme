// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  configFirebase:{
    apiKey: "AIzaSyAEJCNP6rdoKUWZ-ZSS3FuNjpkQ-2a9IGU",
    authDomain: "sueme-d40f6.firebaseapp.com",
    databaseURL: "https://sueme-d40f6.firebaseio.com",
    projectId: "sueme-d40f6",
    storageBucket: "sueme-d40f6.appspot.com",
    messagingSenderId: "985427952319",
    appId: "1:985427952319:web:21ab7a4ec1dc30220ffc0a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
