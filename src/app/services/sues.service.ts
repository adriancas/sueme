import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable } from 'rxjs';
import {map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SuesService {
  suesCollection : AngularFirestoreCollection<any>;
  sues: Observable<any[]>;

  constructor(private readonly afs:AngularFirestore) {
    this.suesCollection = afs.collection<any>('sues');
    this.sues = this.suesCollection.snapshotChanges().pipe(map(actions => actions.map(a => {
      const data = a.payload.doc.data() as any;
      const id = a.payload.doc.id;
      return {id, ...data};
    })));
   }


   getSues(){
     return this.sues;
   }
   updateSue(sue:any){
     console.log(sue.id);
     return this.suesCollection.doc(sue.id).update(sue);
   }

   deleteSue(id:string){
    return this.suesCollection.doc(id).delete();
   }
   createSue(sue:any){
    return this.suesCollection.add(sue);
   }
}
