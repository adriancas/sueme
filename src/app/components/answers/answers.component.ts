import { Component, OnInit } from '@angular/core';
import { SuesService } from './../../services/sues.service';
@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.scss']
})
export class AnswersComponent implements OnInit {
  answers: any[];
  pageActual:number = 1;
  constructor(private sueService: SuesService) { 
    this.answers = [];
  }

  ngOnInit() {
    this.getSues();
  }

  getSues(){
    this.sueService.getSues().subscribe((data)=>{
      this.answers=data;
      console.log("lista answers",this.answers);
    });
  }
}
