import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuesComponent } from './sues.component';

describe('SuesComponent', () => {
  let component: SuesComponent;
  let fixture: ComponentFixture<SuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
