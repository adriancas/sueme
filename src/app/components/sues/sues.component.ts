import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SuesService } from './../../services/sues.service';
@Component({
  selector: 'sues',
  templateUrl: './sues.component.html',
  styleUrls: ['./sues.component.scss']
})
export class SuesComponent implements OnInit {
  
  form: FormGroup;
  constructor(private sueService: SuesService) {

    this.form = new FormGroup({
      question: new FormControl(''),
      answer: new FormControl(''),
      date: new FormControl(''),
      checked: new FormControl(false)
    });
  }

  ngOnInit() {
  }

  onSubmit(form: FormGroup) {
   
    // call service
    this.sueService.createSue(this.form.value);
    this.form.reset();

  }

}
