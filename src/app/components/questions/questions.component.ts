import { Component, OnInit } from '@angular/core';
import { SuesService } from './../../services/sues.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {
  
  form: FormGroup;
  suesList: any[];
  constructor(private sueService: SuesService) {
    this.suesList = [];

    this.form = new FormGroup({
      question: new FormControl(''),
      answer: new FormControl(''),
      date: new FormControl(''),
      checked: new FormControl(false),
      id: new FormControl(false)
    });
  }

  ngOnInit() {
    this.getSues();
  }

  getSues(){
    this.sueService.getSues().subscribe((data)=>{
      this.suesList=data;
      console.log("lista sues",this.suesList);
      
    });
  }

  respond(sues:any,form:FormGroup){

    sues.answer=form.get('answer').value
    sues.checked=true;

    this.sueService.updateSue(sues);
    
    this.form.reset();

    
  }

}
