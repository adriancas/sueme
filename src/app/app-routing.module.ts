import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AnswersComponent} from './components/answers/answers.component';
import {QuestionsComponent} from './components/questions/questions.component';
import {SuesComponent} from './components/sues/sues.component';

const routes: Routes = [
  { path:'answers',component:AnswersComponent},
  { path:'questions',component:QuestionsComponent},
  { path:'home',component:SuesComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

